#!/usr/bin/python
# coding=UTF-8

# ----------------------------------------------------------------------------
#	CUL consol
# ----------------------------------------------------------------------------

import string
import sys
import os
import time
import binascii
import traceback
import subprocess
import re
import logging

device = '/dev/cul868'

def ByteToHex( byteStr ):
	return ''.join( [ "%02X" % ord( x ) for x in str(byteStr) ] ).strip()

def readbytes(number):
	buf = ''
	for i in range(number):
		byte = serialport.read()
		buf += byte

	return buf

def read_cul():
	message = None
	try:
		line = serialport.readline()		
		if line:
			print 'line: ' + line
				
	except OSError, e:
		print "------------------------------------------------"
		traceback.print_exc()

def write_cul(message):
	serialport.write(message)

try:
	import serial
except ImportError:
	print "Error: You need to install Serial extension for Python"
	sys.exit(1)

# Open serial port
try:  
	serialport = serial.Serial(device, 115200, timeout=1)
except:  
	print "Error: Failed to connect on " + device
	sys.exit(1)

already_open = serialport.isOpen()
if not already_open:
	serialport.open()
else:
	serialport.close()
	serialport.open()
	
serialport.flushOutput()
serialport.flushInput()
time.sleep(1)

write_cul("V\n")
write_cul("X18\n")
read_cul()
bytenb = 0
message = ''
while 1:
	byte = serialport.read(1)
	if byte:
		message += byte
		#print str(bytenb) + ': \n' + ByteToHex(byte) + '\n'
		bytenb+=1
	else:
		if message:
			print 'New message of ' + str(len(message)) + ' digits [' + str(bytenb) + '] \n'
			message = ''
		bytenb = 0
